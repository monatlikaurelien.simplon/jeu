package co.simplon.promo18;

public class Check {
    public static boolean plateauFull(int[][] plateau) {
        for (int cptligne = 0; cptligne < plateau.length; cptligne++) {

            for (int cptColone = 0; cptColone < plateau[cptligne].length; cptColone++) {
                if (plateau[cptligne][cptColone] == 0) {
                    return false;
                }
            }

        }
        return true;
    }

    public static boolean checkPlateau(int[][] plateau, int numLigne, int numColone) {
        boolean check = false;
        if (numLigne >= 0 && numLigne < plateau.length && numColone >= 0 && numColone < plateau[numLigne].length) {
            if (plateau[numColone][numLigne] == 0) {
                check = true;
            }

        }
        return check;

    }

    public static void fillCase(int[][] plateau, int numLigne, int numColone, int numeroJoueur) {
        plateau[numColone][numLigne] = numeroJoueur;
    }

    public static boolean equals(int entier1, int entier2, int entier3, int entier4) {
        boolean compare = false;
        if (entier1 == entier2 && entier2 == entier3 && entier3 == entier4) {
            compare = true;
        }
        return compare;
    }

    public static boolean win(int[][] plateau, int numJoueur) {

        // ligne
        for (int cptligne = 0; cptligne < plateau.length; cptligne++) {
            if (equals(plateau[cptligne][0], plateau[cptligne][1], plateau[cptligne][2], numJoueur)) {
                return true;
            }
        }

        // collone
        for (int cptColone = 0; cptColone < plateau[0].length; cptColone++) {
            if (equals(plateau[0][cptColone], plateau[1][cptColone], plateau[2  ][cptColone], numJoueur)) {
                return true;
            }
        }

        // digonale
        if (equals(plateau[0][0], plateau[1][1], plateau[2][2], numJoueur)
                || equals(plateau[2][0], plateau[1][1], plateau[0][2], numJoueur)) {
            return true;
        }
        return false;
    }

}
