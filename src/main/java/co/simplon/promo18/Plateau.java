package co.simplon.promo18;


public class Plateau {
  
  public static int[][] creatPlateau(int nbrows, int nbColums) {
    int[][] tab = new int[nbrows][nbColums];
    return tab;

  }

  public static void displayPlateau(int[][] plateau) {

    System.out.print(" ");
    System.out.print(" ");

    for (int cptColone = 0; cptColone < 3; cptColone++) {
      System.out.print(cptColone + " ");
    }
    System.out.println(" ");
    for (int cptligne = 0; cptligne < plateau.length; cptligne++) {
      System.out.print(cptligne + " ");

      for (int cptColone = 0; cptColone < plateau[cptligne].length; cptColone++) {
        switch (plateau[cptColone][cptligne]) {
          case 0:
            System.out.print("- ");
            break;
          case 1:
            System.out.print("x ");
            break;
          case 2:
            System.out.print("o ");
            break;
          default:
            break;

        }
      }
      System.out.println();
    }
  }
}
    
