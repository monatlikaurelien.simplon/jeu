package co.simplon.promo18;

import java.util.Scanner;

public class Playgame {

  
  private static Scanner scan = new Scanner(System.in);

  public static void play() {
    
    int[][] plateau = Plateau.creatPlateau(3, 3);
    int currentNumJoueur = 1;
    boolean playing = true;
    System.out.println();
    System.out.println("Bienvenue a vous ! Jouons au Morpion !");
    System.out.println();
    System.out.println("Voici les regles du jeu : Le joueur 1 a le symbole 'croix', le joueur 2 a le symbole 'rond' nous jouons sur une grille de 3/3. Choisisez le bon emplacement indiqué, a l'aide des collonnes et des lignes numerotés. Le but du jeu est d'aligner avant son adversaire 3 symboles identiques horizontalement, verticalement ou en diagonale. BONNE CHANCE ! ");
    System.out.println();
    System.out.println("       ------Debut du jeu------      ");
    
    while (playing) {
      System.out.println("\n************Tour du joueur" + currentNumJoueur + "************\n");
      int numLigne = -1;
      int numColone = -1;
      boolean caseValide = false;

      while (!caseValide) {
        Plateau.displayPlateau(plateau);
        System.out.println();
        System.out.print("Le joueur " + currentNumJoueur + "  Choisit une ligne : ");
        numLigne = scan.nextInt();

        System.out.print("Le joueur " + currentNumJoueur + " Choisit une collone : ");
        numColone = scan.nextInt();
        System.out.println();
        System.out.println("     ------1 pour Validez vous votre choix ou 2 pour modifier------      ");
        
        switch (scan.nextInt()) {
          case 1:
          caseValide = Check.checkPlateau(plateau, numLigne, numColone);
          if (!caseValide) {
            System.out.println("Case invalide");
            System.out.println();
          }
          break;
          case 2 :
          break;
        }
      }
      Check.fillCase(plateau, numLigne, numColone, currentNumJoueur);
      boolean winning = Check.win(plateau, currentNumJoueur);
      if (winning) {
        Plateau.displayPlateau(plateau);
        System.out.println();
        System.out.println("Le joueur " + currentNumJoueur + " a gagné");
        playing = false;
        System.out.println();
        System.out.println("    ------Voulez vous rejouer ?------      ");
        System.out.println();
        System.out.print("#####Entrer 1 pour rejouer ou bien entrer 2 pour quiter##### : ");

        switch(scan.nextInt()) {
          case 1:
          play();
          break;
          case 2:
          System.out.println();
          System.out.println("        ------Fin du jeu------      ");
        }

      
      }



      boolean égalité = Check.plateauFull(plateau);
      if (égalité) {
        Plateau.displayPlateau(plateau);
        System.out.println("Égalité ! ");
        playing = false;
      } else {
        if (currentNumJoueur == 1) {
          currentNumJoueur = 2;
        } else {
          currentNumJoueur = 1;
        }
      }
    }
  }
}
